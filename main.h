//
// Created by root on 4/27/20.
//

#ifndef NOSLEEP_MAIN_H
#define NOSLEEP_MAIN_H

typedef int FileDescriptor;
typedef int ErrorCode;
const int NO_ERROR = 0;
const int ERROR_OCCURRED = 1;
const char* FILE_PATH = "/usr/bin/sleep";

#endif //NOSLEEP_MAIN_H
