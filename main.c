#include "main.h"
#include <stdio.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <stdlib.h>

ErrorCode main() {

    FileDescriptor notifier = inotify_init();
    FileDescriptor watched_file = -1;
    struct inotify_event* event = malloc(sizeof(struct inotify_event));

    if (NULL == event) {
        printf("failed to allocated memory for an inotify_watch!!\n");
        goto error_no_free;
    }

    if (-1 == notifier) {
        printf("failed at initiating an inotify object!\n");
        goto error;
    }

    watched_file = inotify_add_watch(notifier, FILE_PATH, IN_ACCESS);
    if (-1 == watched_file) {
        printf("failed at gettting the watch for the file %s\n", FILE_PATH);
        goto error;
    }

    printf("started watching the file %s\n", FILE_PATH);

    if (-1 == read(watched_file, event, sizeof(struct inotify_event))) {
        printf("failed to read from notify object!!!\n");
        goto error;
    }

    printf("%s was accessed!!!\n", FILE_PATH);

    free(event);
    return NO_ERROR;
error:
    free(event);
error_no_free:
    return ERROR_OCCURRED;
}
